---
title: Home
---

<img src="/img/avatar.png" style="max-width:15%;min-width:40px;float:right;" alt="Photo" />

# Mike D'Arcy

I am a computer science PhD student at Northwestern University, currently being advised by [Prof. Doug Downey](http://www.cs.northwestern.edu/~ddowney/). My research interests include natural language processing and machine learning. 


**Contact**  
The best way to reach me is via email: `m.m.darcy@u.northwestern.edu`  



