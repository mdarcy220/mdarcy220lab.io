---
title: Publications
---


### Refereed Conference Proceedings Papers
<ol reversed="reversed">

<li>D'Arcy, Mike, Pooyan Fazli, and Dan Simon. "Safe Navigation in Dynamic, Unknown, Continuous, and Cluttered Environments." In Proceedings of the IEEE International Symposium on Safety, Security, and Rescue Robotics, SSRR 2017, 238-44. Shanghai, China, 2017. <a href="https://doi.org/10.1109/SSRR.2017.8088169">doi: 10.1109/SSRR.2017.8088169</a>.</li>
<li>Patel, Utkarsh, Emre Hatay, Mike D'Arcy, Ghazal Zand, and Pooyan Fazli. "Beam: A Collaborative Autonomous Mobile Service Robot." In Proceedings of the AAAI Fall Symposium on Artificial Intelligence for Human-Robot Interaction, AI-HRI 2017, 126-28. Arlington, Virginia, USA, 2017. [<a href="https://arxiv.org/pdf/1710.06831">PDF</a>].</li>

</ol>

### Workshop Papers
<ol reversed="reversed">

<li>Chen, Michael, Mike D'Arcy, Alisa Liu, Jared Fernandez, and Doug Downey. "CODAH: An Adversarially Authored Question-Answer Dataset for Common Sense." In The Third Workshop on Evaluating Vector Space Representations for NLP (RepEval 2019). Minneapolis, MN, USA, 2019. [<a href="https://arxiv.org/pdf/1904.04365">PDF</a>].</li>
<li>D'Arcy, Mike, Pooyan Fazli, and Dan Simon. "Safe Navigation in Dynamic, Unknown, Continuous, and Cluttered Environments." In Proceedings of the Workshop on Planning, Perception and Navigation for Intelligent Vehicles at the IEEE/RSJ International Conference on Intelligent Robots and Systems, IROS 2017. Vancouver, BC, Canada, 2017. [<a href="https://arxiv.org/pdf/1710.06831">PDF</a>]</li>

</ol>

### Preprints
<ol reversed="reversed">

<li>Hamandi, Mahmoud, Mike D'Arcy, and Pooyan Fazli. "DeepMoTIon: Learning to Navigate Like Humans." arXiv 1803.03719 [cs.RO], 2018. [<a href="https://arxiv.org/pdf/1710.06831">PDF</a>]</li>
<li>Patel, Utkarsh, Emre Hatay, Mike D'Arcy, Ghazal Zand, and Pooyan Fazli. "Setting Up the Beam for Human-Centered Service Tasks." arxiv 1710.06831 [cs.RO], 2017. [<a href="https://arxiv.org/pdf/1710.06831">PDF</a>]</li>

</ol>

